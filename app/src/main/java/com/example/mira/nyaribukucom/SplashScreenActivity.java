package com.example.mira.nyaribukucom;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.mira.nyaribukucom.activity.MainActivity;
import com.example.mira.nyaribukucom.auth.Login;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mira on 2/19/2017.
 */

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            startActivity(new Intent(SplashScreenActivity.this, Login.class));
        finish();
    }

}
