package com.example.mira.nyaribukucom.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mira.nyaribukucom.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Anak_anak extends Fragment {


    public Anak_anak() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_anak_anak, container, false);
    }

}
