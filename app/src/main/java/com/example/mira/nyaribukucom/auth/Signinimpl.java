package com.example.mira.nyaribukucom.auth;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;


/* * Created by Ilham on 2/16/2017.*/



public class Signinimpl {
    DatabaseReference mDatabaseUsers;
    public Signinimpl(DatabaseReference mDatabaseUsers) {
        this.mDatabaseUsers=mDatabaseUsers;
    }

    public void login( final String user_id, final ResultLogin resultLogin){
        mDatabaseUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.hasChild(user_id)){
                    resultLogin.onSuccess();

                }else {
                    resultLogin.elseNyaIf();


                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                resultLogin.onCalcel();
            }
        });
    }
}
